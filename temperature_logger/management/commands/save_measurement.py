from django.core.management.base import BaseCommand, CommandError
from temperature_logger.models import Temperature
from temperature_logger.sensor import auto_device_temperature_celsius


class Command(BaseCommand):
    help = 'Save a new temperature measurement'

    def handle(self, *args, **options):
        try:
            celsius = auto_device_temperature_celsius()
        except ValueError:
            raise CommandError("Not expected value from the sensor")
        else:
            Temperature.objects.create(celsius=celsius)
