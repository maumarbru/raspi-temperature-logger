import datetime
from django.http import JsonResponse, Http404
from django.shortcuts import HttpResponse
from django.utils.timezone import localtime, get_current_timezone_name
from temperature_logger.models import Temperature


def last_measurement_json(request):
    temperature = Temperature.objects.last()
    if temperature:
        if temperature.was_published_recently:
            return JsonResponse({
                'celsius': temperature.celsius,
                'fahrenheit': temperature.fahrenheit,
            })
        else:
            raise Http404("The last temperature meassurement is too all")
    else:
        raise Http404("There's no temperature meassurements")


def measurements_plot(request, last_hours=None):
    import numpy as np
    from bokeh.layouts import gridplot
    from bokeh.plotting import figure, show, output_file
    from bokeh.resources import CDN
    from bokeh.embed import file_html

    N = int(request.GET.get("N", 1))
    M = int(request.GET.get("M", 1))

    if last_hours:
        last_date = Temperature.objects.last().pub_date
        since = last_date - datetime.timedelta(hours=last_hours)
        query = Temperature.objects.filter(pub_date__gte=since)
    else:
        query = Temperature.objects.all()

    temperatures = np.array(query.values_list('pub_date', 'celsius'))
    x, y = temperatures[::M, 0], temperatures[:, 1]
    timezone_name = get_current_timezone_name()
    x = np.array(list(map(localtime, x)))
    y = np.convolve(y, np.ones(N), 'valid') / N
    y = y[::M]
    TOOLS = "pan,xwheel_zoom,box_zoom,reset,save,xpan,xwheel_pan,hover"

    p2 = figure(
        title="Mora",
        tools=TOOLS,
        x_axis_type='datetime',
        x_axis_label='Date [{}]'.format(timezone_name),
        y_axis_label='Temperature [°C]',
        plot_width=1000, plot_height=400,
        # toolbar_location="right",
        # toolbar_sticky=False,
    )
    p2.circle(x, y)
    p2.line(x, y)
    # p = gridplot([[p2]], sizing_mode='stretch_both')

    html = file_html(p2, CDN, "Mora temperatures")
    return HttpResponse(html)
