import datetime
from django.utils import timezone
from django.db import models


class Temperature(models.Model):
    pub_date = models.DateTimeField('date published', auto_now=True)
    celsius = models.FloatField()

    class Meta:
        ordering = ['pub_date']

    def __str__(self):
        return "%.1f °C" % self.celsius

    @property
    def fahrenheit(self):
        return round((self.celsius * 1.8) + 32.0, 1)

    @property
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(seconds=120) <= self.pub_date <= now
