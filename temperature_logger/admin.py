from django.contrib import admin
from .models import Temperature


class TemperatureAdmin(admin.ModelAdmin):
    list_display = ['pub_date', 'celsius', 'fahrenheit']


admin.site.register(Temperature, TemperatureAdmin)
